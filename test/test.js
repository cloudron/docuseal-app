#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    const ADMIN_FIRSTNAME = 'Admin';
    const ADMIN_LASTNAME = 'Admin';
    const ADMIN_COMPANY = 'Cloudron';
    const ADMIN_EMAIL = 'admin@cloudron.local';
    const ADMIN_PASSWORD = 'changeme123';

    const TEMPLATE_NAME = 'test cloudron ' + Math.floor((Math.random() * 100) + 1);

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function registerAdmin() {
        await browser.get('https://' + app.fqdn + '/setup');
        await browser.sleep(2000);
        await browser.findElement(By.id('user_first_name')).sendKeys(ADMIN_FIRSTNAME);
        await browser.sleep(1000);
        await browser.findElement(By.id('user_last_name')).sendKeys(ADMIN_LASTNAME);
        await browser.sleep(1000);
        await browser.findElement(By.id('user_email')).sendKeys(ADMIN_EMAIL);
        await browser.sleep(1000);
        await browser.findElement(By.id('account_name')).sendKeys(ADMIN_COMPANY);
        await browser.sleep(1000);
        await browser.findElement(By.id('user_password')).sendKeys(ADMIN_PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "Submit")]')).click();
        await browser.sleep(2000);

        // skip developer newsletters
        await waitForElement(By.xpath('//h1[contains(., "Developer Newsletters")]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//a[contains(., "Skip")]')).click();
        await browser.sleep(2000);

        // profile initials
        await waitForElement(By.xpath('//span[text()="AA"]'));
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/sign_in');
        await browser.sleep(2000);
        await browser.findElement(By.id('user_email')).sendKeys(ADMIN_EMAIL);
        await browser.sleep(1000);
        await browser.findElement(By.id('user_password')).sendKeys(ADMIN_PASSWORD);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(2000);

        // profile initials
        await waitForElement(By.xpath('//span[text()="AA"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//div[contains(@class, "dropdown")]//label')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "Sign out")]')).click();
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//a[@href="/sign_in"]'));
    }

    async function createTemplate() {
        await browser.get('https://' + app.fqdn + '/templates/new');
        await browser.sleep(2000);
        await browser.findElement(By.id('template_name')).sendKeys(TEMPLATE_NAME);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[contains(., "Create")]')).click();

        await waitForElement(By.xpath('//span[contains(., "' + TEMPLATE_NAME + '")]'));
        // await browser.findElement(By.xpath('//button[contains(., "Save")]')).click();
        // await waitForElement(By.xpath('//h1[contains(., "' + TEMPLATE_NAME + '")]'));
    }

    async function checkPDF() {
        await browser.get('https://' + app.fqdn + '/settings/esign');
        await browser.sleep(2000);
        await waitForElement(By.xpath('//form[@action="/verify_pdf_signature"]'));
        // upload PDF
        await browser.findElement(By.xpath('//input[@id="file"]')).sendKeys(path.resolve(__dirname, 'pdf-sample.pdf'));
        await browser.sleep(1000);
        await waitForElement(By.xpath('//h1[contains(., "PDF Signature")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can register Admin', registerAdmin);
    it('can create template', createTemplate);
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can Admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check PDF', checkPDF);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can Admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check PDF', checkPDF);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check PDF', checkPDF);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () { execSync(`cloudron install --appstore-id co.docuseal.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can register Admin', registerAdmin);
    it('can Admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));
    it('can create template', createTemplate);
    it('check PDF', checkPDF);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can Admin login', login.bind(null, ADMIN_EMAIL, ADMIN_PASSWORD));

    it('check PDF', checkPDF);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

});
