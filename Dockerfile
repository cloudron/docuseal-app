FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

ENV RAILS_ENV=production
ENV NODE_ENV=production

RUN apt-get update && \
    apt-get install -y fontforge-nox libheif1 libvips42 libvips-dev poppler-utils && \
    rm -r /var/cache/apt /var/lib/apt/lists

# https://github.com/docusealco/docuseal/blob/master/Dockerfile#L1
RUN mkdir -p /usr/local/rbenv && curl -LSs "https://github.com/rbenv/rbenv/archive/refs/tags/v1.3.0.tar.gz" | tar -xz -C /usr/local/rbenv --strip-components 1 -f -
ENV PATH /usr/local/rbenv/bin:$PATH
ENV RBENV_ROOT /home/cloudron/rbenv
RUN mkdir -p "$(rbenv root)"/plugins/ruby-build && curl -LSs "https://github.com/rbenv/ruby-build/archive/refs/tags/v20241225.2.tar.gz" | tar -xz -C "$(rbenv root)"/plugins/ruby-build --strip-components 1 -f -

# install specific ruby version
ARG RUBY_VERSION=3.4.1
RUN rbenv install ${RUBY_VERSION}
ENV PATH ${RBENV_ROOT}/versions/${RUBY_VERSION}/bin:$PATH

RUN gem install --no-document shakapacker bundler

# renovate: datasource=github-releases depName=docusealco/docuseal versioning=semver
ARG DOCUSEAL_VERSION=1.9.4

RUN curl -L https://github.com/docusealco/docuseal/archive/refs/tags/${DOCUSEAL_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code && \
    rm -rf /app/code/tmp && ln -s /run/docuseal/tmp /app/code/tmp

RUN npm install -g yarn && yarn install --no-cache --network-timeout 1000000

# https://github.com/rubygems/rubygems/issues/3225
RUN bundle update --bundler && \
    bundle install --no-cache --without development test && \
    bundle clean --force && \
    rm -rf ~/.bundle /usr/local/bundle/cache

# https://github.com/docusealco/docuseal/blob/master/Dockerfile#L72
WORKDIR /app/code/app/public/fonts/
RUN wget https://github.com/satbyy/go-noto-universal/releases/download/v7.0/GoNotoKurrent-Regular.ttf && \
    wget https://github.com/satbyy/go-noto-universal/releases/download/v7.0/GoNotoKurrent-Bold.ttf && \
    wget https://github.com/impallari/DancingScript/raw/master/fonts/DancingScript-Regular.otf && \
    wget https://cdn.jsdelivr.net/gh/notofonts/notofonts.github.io/fonts/NotoSansSymbols2/hinted/ttf/NotoSansSymbols2-Regular.ttf && \
    wget https://github.com/Maxattax97/gnu-freefont/raw/master/ttf/FreeSans.ttf && \
    wget https://github.com/impallari/DancingScript/raw/master/OFL.txt
RUN fontforge -lang=py -c 'font1 = fontforge.open("FreeSans.ttf"); font2 = fontforge.open("NotoSansSymbols2-Regular.ttf"); font1.mergeFonts(font2); font1.generate("FreeSans.ttf")'
# hardcoded to /fonts
RUN cp -rf /app/code/app/public/fonts /
RUN mv ./FreeSans.ttf /usr/share/fonts/freefont
WORKDIR /app/code

RUN bundle exec bootsnap precompile --gemfile app/ lib/
RUN ./bin/shakapacker

ENV WORKDIR=/app/data/docuseal

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

